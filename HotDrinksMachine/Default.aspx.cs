﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using QuickLibs.Javascript;
using QuickLibs.Utils;

namespace HotDrinksMachine
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AjaxHandler.Do("Action",
                "GetItems", (Action)GetItems
            );
        }


        /// <summary>
        /// Simulates an API request for menu itmes, and returns the items in JSON format.
        /// </summary>
        protected void GetItems()
        {
            //Create JSON output, this could be generated from a SQL Data Table.
            JSON response = JSON.Load("~/MenuItems.json");
            response.DoResponse();
        }
    }
}