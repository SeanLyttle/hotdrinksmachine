﻿var IdleTimeout;

$(function () {
    //Buttons
    $("#StartButton").click(function () {
        SwitchView("MenuView");
    });

    $("#NextButton").click(function () {
        SwitchView("MenuView");
        if (IdleTimeout)
            clearTimeout(IdleTimeout);
    });

    //Fade in the home screen immediately.
    SwitchView("HomeView");

    //Load Menu Items
    $.getJSON("", {
        Action: "GetItems"
    }).done(function (response) {
        //If there was a valid response, generate the menue items.
        if (response && response.items && response.items.length)
            GenerateMenu(response.items);
        else { //Otherwise return an error message to the console if available.
            console.error("Error processing menu items: " + response.error || "Invalid response.");
        }
    }).fail(function (xhr, message, error) {
        //Upon failed request, return an errror message to the console.
        console.error("Error loading menu items: " + message);
    })
});


//This function changes the active view on the virtual screen.
function SwitchView(view) {
    $("#ViewCtrl>div:visible").fadeOut(300);
    $("#" + view).delay(300).fadeIn(300);
}

//This function populates the main menu with items loaded from an API / database.
function GenerateMenu(items) {
    //Clear the existing list.
    $("#ItemList").empty();

    //Add each menu item.
    items.forEach(function (item) {
        $("<a>").text(item.name)
            .data("steps", item.steps)
            .addClass("Button Item")            
            .attr("href", "javascript:")
            .click(ItemSelected)
            .appendTo("#ItemList");
    });
}

//This function processes an item when selected
function ItemSelected() {
    //Clear existing output.
    $("#Output").empty();
    $("#Complete").hide();

    //Switch to the processing page.
    SwitchView("ProcessView");

    //Get steps for the current item.
    var steps = $(this).data("steps");

    //Start processing steps after a short delay, to allow for fade in.
    setTimeout(ProcessStep, 500, steps, 0);
}

function ProcessStep(steps, index) {
    var step = steps[index];

    //If step is populated, process that step.
    if (step) {
        //Create output item.
        var item = $("<li>").appendTo("#Output");
        OutputStep(step.desc, item);

        //Execute the next step after the required delay.
        setTimeout(ProcessStep, step.duration * 1000, steps, index + 1);
    }
    else { //Otherwise end the process.
        $("#Complete").show();
        //Return to homepage if no other action is selected.
        IdleTimeout = setTimeout(SwitchView, 30000, "HomeView");
    }

}

//Outputs the description to the screen.
function OutputStep(desc, item) {
    //Add one character at a time.
    item.text(desc.substr(0, item.text().length + 1));

    if (item.text().length < desc.length)
        setTimeout(OutputStep, 50, desc, item)
}