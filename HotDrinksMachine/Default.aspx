﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HotDrinksMachine.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hot Drinks Machine</title>
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <link href="resources/stylesheets/main.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="Content">
            <div id="MachineTop">
                <h1>Hot Drinks Machine</h1>
            </div>
            <div id="MachineMain">
                <div id="ViewPort">
                    <div id="ViewCtrl">
                        <div id="HomeView" class="Hidden">
                            <h2>Your hot drinks,<br />just the way you like it.</h2>
                            <a id="StartButton" href="javascript:" class="Button">Tap to begin</a>
                        </div>
                        <div id="MenuView" class="Hidden">
                            <h3>Please make a selection:</h3>
                            <div id="ItemList">
                                <p>Sorry, there are currently no available products.</p>
                            </div>
                        </div>
                        <div id="ProcessView" class="Hidden">
                            <h3>Processing your order:</h3>
                            <ul id="Output">
                            </ul>
                            <div id="Complete">
                                <p>Your order is ready. Please take your drink.</p>
                                <a id="NextButton" href="javascript:" class="Button">New Order</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="Outlet">

                </div>
            </div>
        </div>
    </form>
    <!-- Page Scripts -->
    <script type="text/javascript" src="resources/javascript/libraries/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="resources/javascript/libraries/jquery-3.5.1.intellisense.js"></script>
    <script type="text/javascript" src="Default.aspx.js"></script>
</body>
</html>
